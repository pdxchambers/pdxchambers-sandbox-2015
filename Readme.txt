=== PDXChambers Sandbox 2015 ===
Contributers: pdxchambers
Tags: two-columns, left-sidebar, featured-images, sticky-post
Requires at least: 4.3.1
Tested up to: 4.3.1

An extremely basic theme intended for use as a starting point for more robust themes.

== Descritption ==
PDXChambers Sandbox 2015 is a bare bones framework to use as a jumping off point for a more robust theme. It can be used as-is or extended to add more functionality.
Out of the box it supports a front-page, has a basic menu, and has been checked against the Theme Checker plugin to ensure
compatibility with WordPress best practices.

The theme uses sticky posts as a way to implement a "featured" post on the front page. It displays the most recent sticky post prominently at the top of the page and ignores subsequent ones,
however sticky posts will display at the top of the blog page normally.

== Known Issues ==
- In the Theme Customizer: Setting the color for the navigation menu links sets the same color for the active link. Setting the color for the active link after setting the link colors displays
properly. This behavior doesn't occur if the transport method is set to "refresh" instead of "PostMessage". Since it is a minor irritation and doesn't impact the actual functionality of the theme,
I'm leaving it to be fixed in a future release.

== Changelog ==
